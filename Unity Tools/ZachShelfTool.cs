﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;
using Sirenix.Utilities;
using System;
using System.Collections.Generic;
using System.Reflection;

public class ZachUntiltyWindow : OdinEditorWindow
    {
    //For opening the menu
    [MenuItem("Tools/ZachsShelf")]
    private static void OpenWindow()
        {
        var window = GetWindow<ZachUntiltyWindow>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(500, 700);
        }

    #region SCENE_CLEANING

    //Clears out any colliders the animators made have added to objects
    [BoxGroup("Clean Scene")]
    [Button("RemoveColliders", buttonSize: ButtonSizes.Medium)]
    private void RemoveColliders()
        {
        List<GameObject> allChildren = new List<GameObject>();

        //Gets all components including children
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            var nextSelection = Selection.gameObjects[i].GetComponentsInChildren<Transform>();
            for (int j = 0; j < nextSelection.Length; j++)
                allChildren.Add(nextSelection[j].gameObject);
            }

        //Remove all that were found
        for (int i = 0; i < allChildren.Count; i++)
            if (allChildren[i].GetComponent<Collider>())
                DestroyImmediate(allChildren[i].GetComponent<Collider>());
        }

    //Used to remove icon that were added for debugging
    [BoxGroup("Clean Scene")]
    [Button("Remove Icons", buttonSize: ButtonSizes.Medium)]
    void RemoveAllIcons()
        {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            DrawIcon(Selection.gameObjects[i], 0);
            for (int j = 0; j < Selection.gameObjects[i].transform.childCount; j++)
                DrawIcon(Selection.gameObjects[i].transform.GetChild(j).gameObject, 0);
            }
        }

    //Used to reset icon back to default
    void DrawIcon(GameObject gameObject, int idx)
        {
        var egu = typeof(EditorGUIUtility);
        var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
        var args = new object[] { gameObject, null };
        var setIcon = egu.GetMethod("SetIconForObject", flags, null, new Type[] { typeof(UnityEngine.Object), typeof(Texture2D) }, null);
        setIcon.Invoke(null, args);
        }

    #endregion

    #region GAMEOBJECT Manipulation
    [OnInspectorGUI]
    private void Space2() { GUILayout.Space(20); }

    //Used in below functions
    [BoxGroup("Utilities")]
    public string stringOne;
    [BoxGroup("Utilities")]
    public string stringTwo;

    //Mass renaming of selected gameobjects
    [BoxGroup("Utilities")]
    [Button("Rename", buttonSize: ButtonSizes.Medium)]
    private void RenameTool()
        {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            Undo.RecordObject(Selection.gameObjects[i], "Rename");
            Selection.gameObjects[i].name = stringOne + (i + 1).ToString("D2");
            }
        }
    //Add prefix to gameobjects
    [BoxGroup("Utilities")]
    [Button("Add Prefix", buttonSize: ButtonSizes.Medium)]
    private void PrefixTool()
        {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            Undo.RecordObject(Selection.gameObjects[i], "RenamePrefix");
            Selection.gameObjects[i].name = stringOne + Selection.gameObjects[i].name;
            }
        }
    //Find and replace function
    [BoxGroup("Utilities")]
    [Button("Find And Replace", buttonSize: ButtonSizes.Medium)]
    private void FindReplaceTool()
        {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            Undo.RecordObject(Selection.gameObjects[i], "ReplaceTool");
            string workingString = Selection.gameObjects[i].name;
            workingString = workingString.Replace(stringOne, stringTwo);
            Selection.gameObjects[i].name = workingString;
            }
        }
    //Remove Unities default increment names
    [BoxGroup("Utilities")]
    [Button("Remove Unity Increments", buttonSize: ButtonSizes.Medium)]
    private void RemoveIncrementsTool()
        {
        for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
            Undo.RecordObject(Selection.gameObjects[i], "RemoveIncrement");
            string workingString = Selection.gameObjects[i].name;
            if (workingString.IndexOf(" (") != -1)
                workingString = workingString.Substring(0, workingString.IndexOf(" ("));
            Selection.gameObjects[i].name = workingString;
            }
        }

    #endregion

    #region STORING AND SNIPPING OBJECTS

    [BoxGroup("Move Objects"), ShowInInspector]
    StoredInformation source;

    [BoxGroup("Move Objects")]
    [Button("Store Source", buttonSize: ButtonSizes.Medium)]
    private void StoreSelectedGameobject()
        {
        if (Selection.transforms.Length == 1)
            {
            source = new StoredInformation(Selection.transforms[0].gameObject.name, Selection.transforms[0].position, Selection.transforms[0].rotation);
            }
        else
            Debug.Log("Select a single transform");
        }

    //Match object's position to stored object
    [BoxGroup("Move Objects")]
    [Button("Move To Source", buttonSize: ButtonSizes.Medium)]
    private void MoveWorldPOS()
        {
        if(source != null)
            {
        if (Selection.transforms.Length >= 1)
            {
            for (int i = 0; i < Selection.transforms.Length; i++)
                {
                Undo.RecordObject(Selection.transforms[i], "Move To Other Spot");
                    Selection.transforms[i].position = source.position;
                }
            }
        else
            Debug.Log("Nothing selected");
            }
        else
            Debug.Log("Need to get a source first");
        }

    //Match object's rotation to stored object
    [BoxGroup("Move Objects")]
    [Button("Rotate To Source", buttonSize: ButtonSizes.Medium)]
    private void RotateWorldPOS()
        {
        if (source != null)
            {
            if (Selection.transforms.Length >= 1)
                {
                for (int i = 0; i < Selection.transforms.Length; i++)
                    {
                    Undo.RecordObject(Selection.transforms[i], "Rotate To Source");
                    Selection.transforms[i].rotation = source.rotation;
                    }
                }
            else
                Debug.Log("Nothing selected");
            }
        else
            Debug.Log("Need to get a source first");
        }
    }

[Serializable]
public class StoredInformation
    {
    [SerializeField]
    public string name;
    [SerializeField]
    public Vector3 position;
    [SerializeField]
    public Quaternion rotation;

    public StoredInformation(string _name, Vector3 _POS, Quaternion _Rotation)
        {
        name = _name;
        position = _POS;
        rotation = _Rotation;
        }
    }
#endregion

#endif