import maya.cmds as maya

if maya.window("tankSelection", ex=True):
    maya.deleteUI("tankSelection", window=True)
    
maya.window("tankSelection", t="Tank Selection", rtf=True, w=200, h=10)
maya.columnLayout("mainColumn", adjustableColumn=True)

maya.separator(st="in")

maya.text(l="Tank UI")

maya.separator(st="in")

maya.frameLayout(l="Reset Keys", vis=True, cc="resetSize()")
maya.gridLayout( numberOfColumns=2, cellWidthHeight=(100, 25) )
maya.button(l="Reset All", c="ResetAllKeys()")
maya.button(l="Reset Selected", c="ResetAllSel()")
maya.setParent("..")
maya.setParent("..")

maya.separator(st="in")

maya.frameLayout(l="Mirror", vis=True, cc="resetSize()")
maya.gridLayout( numberOfColumns=2, cellWidthHeight=(100, 25) )
maya.button(l="All L->R", c="mirrorLeftToRightAll()")
maya.button(l="ALL R->L", c="mirrorRightToLeftAll()")
maya.button(l="SEL L->R", c="mirrorLeftToRightSel()")
maya.button(l="SEL R->L", c="mirrorRightToLeftSel()")
maya.setParent("..")
maya.setParent("..")

maya.separator(st="in")

maya.frameLayout(l="Flip", vis=True, cc="resetSize()")
maya.gridLayout( numberOfColumns=2, cellWidthHeight=(100, 25) )
maya.button(l="All L->R", c="flipLeftToRightAll()")
maya.button(l="ALL R->L", c="flipRightToLeftAll()")
maya.button(l="SEL L->R", c="flipLeftToRightSel()")
maya.button(l="SEL R->L", c="flipRightToLeftSel()")
maya.setParent("..")
maya.setParent("..")

maya.separator(st="in")

maya.frameLayout(collapsable = True, collapse=True, l="Controls Visibility", cc="resetSize()")
maya.columnLayout(adjustableColumn=True)
maya.checkBox("cB_WheelControls", l="Wheel Controls", cc="wheelChange()", v=int(maya.getAttr("VisibleGroups.wheelControls")))
maya.checkBox("cB_TurretControls", l="Turret Controls", cc="turretChange()", v=int(maya.getAttr("VisibleGroups.turretControl")))
maya.checkBox("cB_SpeakerControls", l="Speaker Controls", cc="speakerChange()", v=int(maya.getAttr("VisibleGroups.speakerControls")))
maya.checkBox("cB_HeadphoneControls", l="Headphone Controls", cc="headphoneChange()", v=int(maya.getAttr("VisibleGroups.headphoneControl")))
maya.checkBox("cB_BankControls", l="Bank Controls", cc="bankChange()", v=int(maya.getAttr("VisibleGroups.bankControls")))
maya.setParent("..")
maya.setParent("..")

maya.frameLayout(collapsable = True, collapse=True, l="Geomentry Visibility",  cc="resetSize()")
maya.columnLayout(adjustableColumn=True)
maya.checkBox("cB_wheelCover", l="Wheel Cover", cc="wheelCoverChange()", v=int(maya.getAttr("VisibleGroups.wheelCover")))
maya.checkBox("cB_TankBase", l="Tank Base", cc="tankBaseChange()", v=int(maya.getAttr("VisibleGroups.tankBase")))
maya.checkBox("cB_headphones", l="Headphones", cc="headphoneMeshChange()", v=int(maya.getAttr("VisibleGroups.headphones")))
maya.checkBox("cB_turret", l="Turret", cc="turretMeshChange()", v=int(maya.getAttr("VisibleGroups.turret")))
maya.checkBox("cB_L_Treads", l="Left Treads", cc="lTreadsChange()", v=int(maya.getAttr("VisibleGroups.treadsL")))
maya.checkBox("cB_R_Treads", l="Right Treads", cc="rTreadsChange()", v=int(maya.getAttr("VisibleGroups.treadsR")))
maya.checkBox("cB_L_Wheels", l="Left Wheels", cc="lWheelsChange()", v=int(maya.getAttr("VisibleGroups.wheelsL")))
maya.checkBox("cB_R_Wheels", l="Right Wheels", cc="rWheelsChange()", v=int(maya.getAttr("VisibleGroups.wheelsR")))
maya.checkBox("cB_Speakers", l="Speakers", cc="speakerMeshChange()", v=int(maya.getAttr("VisibleGroups.speaker")))
maya.setParent("..")
maya.setParent("..")

maya.separator(vis=False)

maya.columnLayout("mainColumn", e=True, rs=10, cat=("both", 5))

maya.showWindow("tankSelection")

#General functions
def resetSize():
    maya.window("tankSelection", e=True, h=1)

#Control visiblity Functions

def wheelChange():
    if maya.checkBox("cB_WheelControls", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.wheelControls", 1)
    else :
        maya.setAttr("VisibleGroups.wheelControls", 0)
        
def turretChange():
    if maya.checkBox("cB_TurretControls", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.turretControl", 1)
    else :
        maya.setAttr("VisibleGroups.turretControl", 0)
        
def speakerChange():
    if maya.checkBox("cB_SpeakerControls", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.speakerControls", 1)
    else :
        maya.setAttr("VisibleGroups.speakerControls", 0)
        
def headphoneChange():
    if maya.checkBox("cB_HeadphoneControls", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.headphoneControl", 1)
    else :
        maya.setAttr("VisibleGroups.headphoneControl", 0)
        
def bankChange():
    if maya.checkBox("cB_BankControls", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.bankControls", 1)
    else :
        maya.setAttr("VisibleGroups.bankControls", 0)
        
#Mesh visibility functions

def wheelCoverChange():
    if maya.checkBox("cB_wheelCover", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.wheelCover", 1)
    else :
        maya.setAttr("VisibleGroups.wheelCover", 0)
        
def tankBaseChange():
    if maya.checkBox("cB_TankBase", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.tankBase", 1)
    else :
        maya.setAttr("VisibleGroups.tankBase", 0)
        
def headphoneMeshChange():
    if maya.checkBox("cB_headphones", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.headphones", 1)
    else :
        maya.setAttr("VisibleGroups.headphones", 0)
        
def turretMeshChange():
    if maya.checkBox("cB_turret", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.turret", 1)
    else :
        maya.setAttr("VisibleGroups.turret", 0)
        
def lTreadsChange():
    if maya.checkBox("cB_L_Treads", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.treadsL", 1)
    else :
        maya.setAttr("VisibleGroups.treadsL", 0)
        
def rTreadsChange():
    if maya.checkBox("cB_R_Treads", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.treadsR", 1)
    else :
        maya.setAttr("VisibleGroups.treadsR", 0)
        
def lWheelsChange():
    if maya.checkBox("cB_L_Wheels", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.wheelsL", 1)
    else :
        maya.setAttr("VisibleGroups.wheelsL", 0)
        
def rWheelsChange():
    if maya.checkBox("cB_R_Wheels", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.wheelsR", 1)
    else :
        maya.setAttr("VisibleGroups.wheelsR", 0)
        
def speakerMeshChange():
    if maya.checkBox("cB_Speakers", q=True, v=True) == 1:
        maya.setAttr("VisibleGroups.speaker", 1)
    else :
        maya.setAttr("VisibleGroups.speaker", 0)
        
#Mirror Tools
def mirrorLeftToRightSel():

    selPose = maya.ls(sl=True)

    if len(selPose) < 1:
        maya.warning("Must select at least one object")
    else:
        for all in selPose:
            keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
            if "_L_" in all:
                for values in keyable:
                    newTar = all.replace("_L_", "_R_")
                    findVal = maya.getAttr(all + "." + values)
                    if values == "translateX":
                        findVal *= -1  
                    if values == "rotateX":
                        findVal *= -1  
                    maya.setAttr((newTar + "." + values), findVal)
                    
def mirrorRightToLeftSel():

    selPose = maya.ls(sl=True)

    if len(selPose) < 1:
        maya.warning("Must select at least one object")
    else:
        for all in selPose:
            keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
            if "_R_" in all:
                for values in keyable:
                    newTar = all.replace("_R_", "_L_")
                    findVal = maya.getAttr(all + "." + values)
                    if values == "translateX":
                        findVal *= -1  
                    if values == "rotateX":
                        findVal *= -1  
                    maya.setAttr((newTar + "." + values), findVal)
                    
def mirrorRightToLeftAll():

    selPose = maya.ls()

    for all in selPose:
        keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
        if "CC_R_" in all:
            if "Shape" not in all:
                for values in keyable:
                    newTar = all.replace("_R_", "_L_")
                    findVal = maya.getAttr(all + "." + values)
                    if values == "translateX":
                        findVal *= -1 
                    if values == "rotateX":
                        findVal *= -1  
                    maya.setAttr((newTar + "." + values), findVal)
                    
def mirrorLeftToRightAll():

    selPose = maya.ls()

    for all in selPose:
        keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
        if "CC_L_" in all:
            if "Shape" not in all:
                for values in keyable:
                    newTar = all.replace("_L_", "_R_")
                    findVal = maya.getAttr(all + "." + values)
                    if values == "translateX":
                        findVal *= -1  
                    if values == "rotateX":
                        findVal *= -1  
                    maya.setAttr((newTar + "." + values), findVal)
                    
#Flip Tools
def flipLeftToRightSel():

    selPose = maya.ls(sl=True)

    if len(selPose) < 1:
        maya.warning("Must select at least one object")
    else:
        for all in selPose:
            keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
            if "_L_" in all:
                for values in keyable:
                    newTar = all.replace("_L_", "_R_")
                    findValOrgin = maya.getAttr(all + "." + values)
                    findValTar = maya.getAttr(newTar + "." + values)
                    if values == "translateX":
                        findValOrgin *= -1
                        findValTar *= -1 
                    if values == "rotateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    maya.setAttr((newTar + "." + values), findValOrgin)
                    maya.setAttr((all + "." + values), findValTar)
                    
def flipRightToLeftSel():

    selPose = maya.ls(sl=True)

    if len(selPose) < 1:
        maya.warning("Must select at least one object")
    else:
        for all in selPose:
            keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
            if "_R_" in all:
                for values in keyable:
                    newTar = all.replace("_R_", "_L_")
                    findValOrgin = maya.getAttr(all + "." + values)
                    findValTar = maya.getAttr(newTar + "." + values)
                    if values == "translateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    if values == "rotateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    maya.setAttr((newTar + "." + values), findValOrgin)
                    maya.setAttr((all + "." + values), findValTar)
                    
def flipRightToLeftAll():

    selPose = maya.ls()

    for all in selPose:
        keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
        if "CC_R_" in all:
            if "Shape" not in all:
                for values in keyable:
                    newTar = all.replace("_R_", "_L_")
                    findValOrgin = maya.getAttr(all + "." + values)
                    findValTar = maya.getAttr(newTar + "." + values)
                    if values == "translateX":
                        findValOrgin *= -1
                        findValTar *= -1 
                    if values == "rotateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    maya.setAttr((newTar + "." + values), findValOrgin)
                    maya.setAttr((all + "." + values), findValTar)
                    
def flipLeftToRightAll():

    selPose = maya.ls()

    for all in selPose:
        keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
        if "CC_L_" in all:
            if "Shape" not in all:
                for values in keyable:
                    newTar = all.replace("_L_", "_R_")
                    findValOrgin = maya.getAttr(all + "." + values)
                    findValTar = maya.getAttr(newTar + "." + values)
                    if values == "translateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    if values == "rotateX":
                        findValOrgin *= -1
                        findValTar *= -1  
                    maya.setAttr((newTar + "." + values), findValOrgin)
                    maya.setAttr((all + "." + values), findValTar)
                    
#Key Resets
def ResetAllKeys():

    selPose = maya.ls()

    for all in selPose:
        keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
        if "CC_" in all:
            if "Shape" not in all:
                for values in keyable:
                    if "scale" in values :
                        maya.setAttr((all + "." + values), 1)
                    else :
                        maya.setAttr((all + "." + values), 0)
        if "anim_" in all:
            if "Shape" not in all:
                for values in keyable:
                    if "scale" in values :
                        maya.setAttr((all + "." + values), 1)
                    else :
                        maya.setAttr((all + "." + values), 0)
                        
def ResetAllSel():

    selPose = maya.ls(sl=True)

    if len(selPose) < 1:
        maya.warning("Must select at least one object")
    else:    
        for all in selPose:
            keyable = maya.listAttr(all, k=True, r=True, w=True, c=True, u=True)
            if "CC_" in all:
                if "Shape" not in all:
                    for values in keyable:
                        if "scale" in values :
                            maya.setAttr((all + "." + values), 1)
                        else :
                            maya.setAttr((all + "." + values), 0)
            if "anim_" in all:
                if "Shape" not in all:
                    for values in keyable:
                        if "scale" in values :
                            maya.setAttr((all + "." + values), 1)
                        else :
                            maya.setAttr((all + "." + values), 0)

    
    